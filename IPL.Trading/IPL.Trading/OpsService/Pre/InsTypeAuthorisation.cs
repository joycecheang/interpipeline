using System;

using Olf.Embedded.Application;
using Olf.Embedded.Trading;

using Olf.Embedded.Generic;
using Olf.Openrisk.Table;
using Olf.Openrisk.IO;
using Olf.Openrisk.Trading;
using Olf.Openrisk.Application;
using Kwa.Core.Logger;

namespace IPL.Trading.OpsService.Pre
{
    [ScriptCategoriesAttribute(EnumScriptCategory.OpsSvcTrade)]

    public class InsTypeAuthorisation : Olf.Embedded.Trading.AbstractTradeProcessListener
    {
        public static String FAILED_MESSAGE = "Please check that you have the correct functional group\nto create or update this instrument type.";
        private KLogger logger = null;
        /// <summary>
        /// This is the Plugin entry point.
        /// A pre process script that validates between user_ins_type_authorization table and personnel functional group
        /// </summary>
        public override PreProcessResult PreProcess(Context context, EnumTranStatus targetStatus,
            ITradeProcessListener.IPreProcessingInfo<EnumTranStatus>[] info, Table clientData)
        {
            logger = KLogger.GetLogger();

            IOFactory ioFactory = context.IOFactory;
            Debug debug = context.Debug;
            
            String sql = "select * from USER_ins_type_authorization uita " +
                    "\n join functional_group fg on fg.name = uita.functional_group_name" +
                    "\n join personnel_functional_group pfg on pfg.func_group_id = fg.id_number and personnel_id = {0}" +
                    "\n where uita.ins_type_name='{1}'";

            foreach (var i in info)
            {
                Transaction tran = i.Transaction;
                String insType = tran.GetValueAsString(EnumTransactionFieldId.InstrumentType);
                sql = String.Format(sql, Olf.NetToolkit.Ref.GetUserId(), insType);

                if (debug.GetDebugLevel() != EnumDebugLevel.Disabled)
                {
                    logger.Info("ENUMTRANSACTIONSTATUS: " + i.ToString());
                    logger.Info("GetFieldName Instrument Type: " + insType);
                    logger.Info("Ref getUserId: " + Olf.NetToolkit.Ref.GetUserId());
                    logger.Info("SQL: " + sql);
                }

                Table ocTable = ioFactory.RunSQL(sql);

                using (ocTable)
                {

                    if (ocTable.RowCount == 0)
                    {

                        return PreProcessResult.Failed(FAILED_MESSAGE);
                    } 
                }
            }

            return PreProcessResult.Succeeded();
        }


    }
       
}
